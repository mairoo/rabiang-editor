import os
from setuptools import setup, find_packages

with open(os.path.join(os.path.dirname(__file__), 'README.md')) as readme:
    README = readme.read()

setup(
    name='rabiang-editor',
    version='0.0.1',
    description='Django widgets of WYSIWYG editor',
    long_description=README,
    author="Jonghwa Seo",
    author_email='pincoins@gmail.com',
    url='https://gitlab.com/mairoo/rabiang-editor',
    license='MIT',
    packages=find_packages(),
    include_package_data=True,
    install_requires=['setuptools'],
    zip_safe=False,
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
    keywords='django,admin,wysiwyg,editor',
)
