# Django widgets for WYSIWYG editors

The custom field is quite easy to extend and reusable.
However, it's quite difficult to switch or remove the field after migration.

I decided to use custom widget for this purpose. Of course, you will have to implement most of features by yourself.

# Install

```
pip install rabiang-editor-0.0.1.tar.gz
```

# Editors

## SimpleMDE

* Website: https://simplemde.com/
* Source: https://github.com/sparksuite/simplemde-markdown-editor
* Version: 1.11.2

### Settings

settings.py

```
INSTALLED_APPS += [    
    'rabiang_editor',
]

SIMPLEMDE_OPTIONS = {
    'autofocus': False,
    'forceSync': True,
    'hideIcons': ['preview', 'side-by-side'],
    'indentWithTabs': False,
    'lineWrapping': True,
    'promptURLs': True,
    'renderingConfig': {
        'singleLineBreaks': False,
        'codeSyntaxHighlighting': True
    },
    'showIcons': ['code', 'table', 'strikethrough', 'horizontal-rule'],
    'spellChecker': False,
}
```

You may set SimpleMDE options in settings.py as listed above. Python dictionary is dumped to Javascript json.
Read [SimpleMDE Configurations](https://github.com/NextStepWebs/simplemde-markdown-editor#configuration) more in detail

### Admin page

#### forms.py

```
from django import forms
from django.conf import settings

from rabiang_editor.rabiang_editor.widgets import SimpleMDEWidget
from .models import Post


class PostAdminForm(forms.ModelForm):
    class Meta:
        model = Post
        exclude = []
        options = getattr(settings, "SIMPLEMDE_OPTIONS", '')
        widgets = {
            'content': SimpleMDEWidget(wrapper_class='simplemde-box-admin', options=options),
        }
```

#### admin.py

```
from django.contrib import admin

from .forms import PostAdminForm
from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ('title',)
    form = PostAdminForm
    
    class Media:
        css = {
            'all': ('css/admin/simplemde.css',)
        }


admin.site.register(Post, PostAdmin)
```

#### static/css/admin/simplemde.css

```
.simplemde-box-admin {
    /* prevent from overlapping left label in admin page */
    position: relative;
    float: left;
    width: 800px;
}

.CodeMirror {
    /* !important because it is located at the top of admin page. */
    font-family: "Helvetica Nene", Helvetica, Arial, "malgun gothic", dotum, sans-serif !important;
    font-size: 15px !important;
    line-height: 1.5 !important;

    /* make it scrollable (default: auto-growing) */
    height: 400px !important;
}
```

### User page

#### forms.py

```
from django import forms
from django.conf import settings

from rabiang_editor.rabiang_editor.widgets import SimpleMDEWidget
from .models import Post


class MessageForm(forms.ModelForm):
    content = forms.CharField(
        label=_('Content'),
        required=True,
        widget=SimpleMDEWidget(
            attrs={
                'class': 'form-control',
                'placeholder': _('Content'),
                'required': 'True',
            },
            wrapper_class='simplemde-box',
            options=getattr(settings, "SIMPLEMDE_OPTIONS", '')
        ),
    )
```

#### message_form.html

```
{{ form.media }}

<link rel="stylesheet" href="{% static "css/simplemde.css" %}">

<form class="form-horizontal" action="." method="post">
{% csrf_token %}
{{ form.content }}
</form>
```

#### static/css/simplemde.css

```
.simplemde-box {
    /* prevent from overlapping left label */
    position: relative;
    float: left;
    width: 620px;
}

.CodeMirror {
    font-family: "Helvetica Nene", Helvetica, Arial, "malgun gothic", dotum, sans-serif;
    font-size: 15px;
    line-height: 1.5;

    /* make it scrollable (default: auto-growing) */
    height: 400px;
}
```

## Summernote

* Website: http://summernote.org/
* Source: https://github.com/summernote/summernote
