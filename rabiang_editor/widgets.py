import json

from django.forms import widgets
from django.template import loader
from django.utils.safestring import mark_safe


class SimpleMDEWidget(widgets.Textarea):
    template_name = 'simplemde/simplemde.html'

    class Media:
        css = {'all': ('simplemde/simplemde.min.css',)}
        js = ('simplemde/simplemde.min.js',)

    def __init__(self, attrs=None, wrapper_class='simplemde-box', options=None):
        self.wrapper_class = wrapper_class
        self.options = options
        super(SimpleMDEWidget, self).__init__(attrs=attrs)

    def render(self, name, value, attrs=None, renderer=None):
        context = {
            'widget': {
                'name': name,
                'value': value,
                'wrapper_class': self.wrapper_class,
                'options': json.dumps(self.options),
            }
        }
        template = loader.get_template(self.template_name).render(context)
        return mark_safe(template)


class SummernoteWidget(widgets.Widget):
    template_name = 'summernote/summernote.html'

    class Media:
        css = {
            'all': (
                'summernote/bootstrap.min.css',
                'summernote/summernote.css',
            )
        }
        js = (
            # To avoid conflicts with user-supplied scripts or libraries,
            # Django’s jQuery (version 2.x) is namespaced as django.jQuery.
            '//ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js',
            'summernote/bootstrap.min.js',
            'summernote/summernote.min.js',
        )

    def __init__(self, attrs=None, wrapper_class='summernote-box', options=None):
        self.wrapper_class = wrapper_class
        self.options = options
        super(SummernoteWidget, self).__init__(attrs=attrs)

    def render(self, name, value, attrs=None, renderer=None):
        context = {
            'widget': {
                'name': name,
                'value': value,
                'wrapper_class': self.wrapper_class,
                'options': json.dumps(self.options),
            }
        }
        template = loader.get_template(self.template_name).render(context)
        return mark_safe(template)
