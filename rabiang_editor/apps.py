from django.apps import AppConfig


class RabiangEditorConfig(AppConfig):
    name = 'rabiang_editor'
    verbose_name = 'Rabiang Editor'
